{
    "id": "171fedda-601b-4059-91f6-b2d1bac099b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_road",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c838911-1d05-4622-9e66-fac722287f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171fedda-601b-4059-91f6-b2d1bac099b3",
            "compositeImage": {
                "id": "c16923d5-fac9-42b2-99d2-8bff70dd061c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c838911-1d05-4622-9e66-fac722287f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff0792fb-68b8-4a0c-966f-d9c25313481b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c838911-1d05-4622-9e66-fac722287f9b",
                    "LayerId": "5f492acd-8ea8-4e91-ac54-631829186cd8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5f492acd-8ea8-4e91-ac54-631829186cd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "171fedda-601b-4059-91f6-b2d1bac099b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}