{
    "id": "f08e16d5-821e-4332-8f5e-e0db6c852de9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_civilian",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 259,
    "bbox_left": 48,
    "bbox_right": 229,
    "bbox_top": -5,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8dcc6b3-e383-4ee6-998c-90b88bd20cac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f08e16d5-821e-4332-8f5e-e0db6c852de9",
            "compositeImage": {
                "id": "ab4cec44-6ea7-47a9-b8da-4e63b99aa305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8dcc6b3-e383-4ee6-998c-90b88bd20cac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7420abe8-0783-4049-962a-109a5c7113a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8dcc6b3-e383-4ee6-998c-90b88bd20cac",
                    "LayerId": "8415ba7c-99f7-4ef4-b7fc-45c449da03b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8415ba7c-99f7-4ef4-b7fc-45c449da03b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f08e16d5-821e-4332-8f5e-e0db6c852de9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}