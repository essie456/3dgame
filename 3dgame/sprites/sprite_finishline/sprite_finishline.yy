{
    "id": "661a386e-3ae0-40f3-9f8f-5db3b94a5568",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_finishline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2410ee15-a673-45b3-a66d-ee983e553f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "661a386e-3ae0-40f3-9f8f-5db3b94a5568",
            "compositeImage": {
                "id": "13392f16-acfe-409d-96d8-6616c2dc04ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2410ee15-a673-45b3-a66d-ee983e553f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07a84fe2-320d-4522-a2cf-0f1e825b0e14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2410ee15-a673-45b3-a66d-ee983e553f09",
                    "LayerId": "1cd6284a-ac33-4aab-a714-8a35cc769d12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1cd6284a-ac33-4aab-a714-8a35cc769d12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "661a386e-3ae0-40f3-9f8f-5db3b94a5568",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}