{
    "id": "4fae6692-4fa9-4232-b30f-b756873af494",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_dotline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44ecc46e-76f0-4e03-b0ea-937792bb6965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fae6692-4fa9-4232-b30f-b756873af494",
            "compositeImage": {
                "id": "56a13500-03c3-4076-ae52-44d074e0e562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44ecc46e-76f0-4e03-b0ea-937792bb6965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b5f77f9-d135-4b6d-9cef-a867357dea2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44ecc46e-76f0-4e03-b0ea-937792bb6965",
                    "LayerId": "5af9f4d5-e2f9-4041-b3db-64dde00f7d9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5af9f4d5-e2f9-4041-b3db-64dde00f7d9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4fae6692-4fa9-4232-b30f-b756873af494",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}