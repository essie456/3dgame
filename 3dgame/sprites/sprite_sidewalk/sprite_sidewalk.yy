{
    "id": "329c14a7-365a-49f1-bcdc-d8eaee0b8c4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_sidewalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2722af2-d3d6-4213-a27d-f62be9535ce5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "329c14a7-365a-49f1-bcdc-d8eaee0b8c4b",
            "compositeImage": {
                "id": "69324617-c391-4fc6-9a2e-b8035ea04dbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2722af2-d3d6-4213-a27d-f62be9535ce5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "472cd331-b07e-4277-b770-9a7aefdd65b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2722af2-d3d6-4213-a27d-f62be9535ce5",
                    "LayerId": "fcd4afbf-62b8-47e0-b9ae-9555f5856e5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fcd4afbf-62b8-47e0-b9ae-9555f5856e5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "329c14a7-365a-49f1-bcdc-d8eaee0b8c4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}