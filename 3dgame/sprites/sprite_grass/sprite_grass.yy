{
    "id": "f086dbe6-392f-420c-ab25-f3ed47b0a475",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e05b0198-397a-456f-a35e-d63620d5513e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f086dbe6-392f-420c-ab25-f3ed47b0a475",
            "compositeImage": {
                "id": "2525ca03-3b84-4646-9d9a-d8a3381981d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e05b0198-397a-456f-a35e-d63620d5513e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59e7cc44-fc74-4147-8278-9ace87e5983a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05b0198-397a-456f-a35e-d63620d5513e",
                    "LayerId": "820a6cd4-06ee-42b3-b66d-09d91373def2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "820a6cd4-06ee-42b3-b66d-09d91373def2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f086dbe6-392f-420c-ab25-f3ed47b0a475",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}