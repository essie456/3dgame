{
    "id": "884b456a-ec2f-46c6-8f78-19ee9f816435",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_car",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 20,
    "bbox_right": 235,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4342937b-f0e6-4d4e-a4a8-b864c14407fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "884b456a-ec2f-46c6-8f78-19ee9f816435",
            "compositeImage": {
                "id": "66f7b26e-1fee-4078-9fab-031b2192f9ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4342937b-f0e6-4d4e-a4a8-b864c14407fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5b0e07a-c55a-43c3-bd85-80fb6a5da49c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4342937b-f0e6-4d4e-a4a8-b864c14407fe",
                    "LayerId": "150474b1-fc4d-46fb-9723-5146aa7d9cd8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "150474b1-fc4d-46fb-9723-5146aa7d9cd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "884b456a-ec2f-46c6-8f78-19ee9f816435",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}